import {View, Text, Dimensions, Image, ImageBackground} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {useIsFocused} from '@react-navigation/native';
import Carousel from 'react-native-snap-carousel';
import ApiProvider from '../data/api/ApiProvider';
import Helper from '../util/Helper';

const Detail = ({navigation, route}) => {
  const [dataMobil, setDataMobil] = useState(null);
  const isFocused = useIsFocused();
  const [isLoading, setIsLoading] = useState(false);
  const [mobil, setMobil] = useState({});

  const SLIDER_WIDTH = Dimensions.get('window').width;
  const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
  const isCarousel = useRef(null);

  useEffect(() => {
    if (route.params) {
      console.log('params : ', route.params);
      setMobil(route.params);
    }
    getDataMobil();
  }, []);

  const getDataMobil = async () => {
    setIsLoading(true);

    const res = await ApiProvider.getDataMobil();

    if (res?.items?.length) {
      setDataMobil(res.items);
    } else {
      alert('Gagal Mengambil Data');
    }

    setIsLoading(false);
  };

  const _renderItem = ({item, index}) => {
    return (
      <View
        style={{
          backgroundColor: 'white',
          borderRadius: 8,
          width: ITEM_WIDTH,
          paddingBottom: 40,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: 0.29,
          shadowRadius: 4.65,
          elevation: 7,
        }}>
        <Image
          style={{
            width: ITEM_WIDTH,
            height: 200,
            borderRadius: 8,
          }}
          source={{uri: item.unitImage}}
        />
        <View
          style={{
            width: '70%',
            paddingHorizontal: 10,
            marginTop: 10,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
              Nama Mobil :
            </Text>
            <Text
              numberOfLines={1}
              style={{fontSize: 14, color: '#000', width: '60%'}}>
              {' '}
              {item.title}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
              Total KM :
            </Text>
            <Text style={{fontSize: 14, color: '#000'}}> {item.totalKM}</Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
              Harga Mobil :
            </Text>
            <Text style={{fontSize: 14, color: '#000'}}>
              {' '}
              {item.harga === undefined
                ? item.harga
                : Helper.convertCurrency(item.harga, 'Rp. ')}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View>
      <View
        style={{
          alignSelf: 'center',
          marginTop: 15,
          marginBottom: 20,
          borderColor: '#dedede',
          borderWidth: 1,
          borderRadius: 6,
          padding: 12,
        }}>
        <Image
          style={{
            width: Dimensions.get('window').width - 10,
            height: 250,
            borderRadius: 8,
          }}
          source={{uri: mobil.unitImage}}
        />
        <View
          style={{
            width: '70%',
            paddingHorizontal: 10,
            marginTop: 10,
          }}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
              Nama Mobil :
            </Text>
            <Text
              numberOfLines={1}
              style={{fontSize: 14, color: '#000', width: '60%'}}>
              {' '}
              {mobil.title}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
              Total KM :
            </Text>
            <Text style={{fontSize: 14, color: '#000'}}> {mobil.totalKM}</Text>
          </View>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
              Harga Mobil :
            </Text>
            <Text style={{fontSize: 14, color: '#000'}}>
              {' '}
              {mobil.harga === undefined
                ? mobil.harga
                : Helper.convertCurrency(mobil.harga, 'Rp. ')}
            </Text>
          </View>
        </View>
      </View>
      <Carousel
        layout={'stack'}
        ref={isCarousel}
        data={dataMobil}
        renderItem={_renderItem}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        useScrollView={true}
      />
    </View>
  );
};

export default Detail;

import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
  Modal,
  ActivityIndicator,
  TextInput,
  StyleSheet,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './Url';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useIsFocused} from '@react-navigation/native';
import AddData from './AddData';
import Axios from 'axios';
import ApiProvider from '../data/api/ApiProvider';
import Helper from '../util/Helper';

const Home = ({navigation, route}) => {
  const [dataMobil, setDataMobil] = useState(null);
  const isFocused = useIsFocused();
  const [isLoading, setIsLoading] = useState(false);
  const [selected, setSelected] = useState({});
  const [showModal, setShowModal] = useState(false);
  const [showAdd, setShowAdd] = useState(false);
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  useEffect(() => {
    getDataMobil();
  }, [isFocused]);

  const getDataMobil = async () => {
    setIsLoading(true);

    const res = await ApiProvider.getDataMobil();
    console.log('response get : ', res);
    console.log('response get item : ', res?.items);

    if (res?.items?.length) {
      setDataMobil(res.items);
    } else {
      alert('Gagal Mengambil Data');
    }

    setIsLoading(false);
  };

  const deleteData = async item => {
    setIsLoading(true);
    const body = [
      {
        _uuid: item._uuid,
      },
    ];

    const res = await ApiProvider.deleteDataMobil(body);
    console.log('response DELETE : ', res);
    if (res?.items?.length) {
      alert('Data Mobil berhasil dihapus');
    } else {
      alert('Gagal Hapus');
    }

    setIsLoading(false);
    setShowAdd(false);
    setHargaMobil('');
    setTotalKM('');
    setNamaMobil('');
    getDataMobil();
  };

  const postData = async () => {
    setIsLoading(true);
    const body = [
      {
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const res = await ApiProvider.addDataMobil(body);
    console.log('response ADD : ', res);

    if (res.status === 200 || res.status === 201) {
      alert('Data Mobil berhasil ditambah');
    } else {
      alert('Gagal Tamabh');
    }

    setIsLoading(false);
    setShowAdd(false);
    getDataMobil();
  };

  const disableBtn = () => {
    return (
      namaMobil === '' ||
      hargaMobil === '' ||
      totalKM === '' ||
      hargaMobil < '100000000'
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home screen
      </Text>
      <FlatList
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => {
              setSelected(item);
              setShowModal(true);
            }}
            onLongPress={() => {
              navigation.navigate('Detail', item);
            }}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: '30%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{width: '90%', height: 100, resizeMode: 'contain'}}
                source={{uri: item.unitImage}}
              />
            </View>
            <View
              style={{
                width: '70%',
                paddingHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text
                  numberOfLines={1}
                  style={{fontSize: 14, color: '#000', width: '60%'}}>
                  {' '}
                  {item.title}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.totalKM}
                </Text>
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Text style={{fontWeight: '700', fontSize: 14, color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {' '}
                  {item.harga === undefined
                    ? item.harga
                    : Helper.convertCurrency(item.harga, 'Rp. ')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 30,
          right: 10,
          width: 40,
          height: 40,
          borderRadius: 20,
          backgroundColor: 'red',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() =>
          //navigation.navigate('AddData')
          setShowAdd(true)
        }>
        <Icon name="plus" size={20} color="#fff" />
      </TouchableOpacity>
      <Modal visible={isLoading} transparent={true}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(0,0,0,0.4)',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              padding: 10,
              borderRadius: 10,
            }}>
            <ActivityIndicator size="large" />
            <Text style={{alignSelf: 'center', marginTop: 10}}>Loading</Text>
          </View>
        </View>
      </Modal>
      <Modal visible={showModal} transparent={true}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.4)',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              width: 250,
              height: 100,
              padding: 10,
              borderRadius: 10,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
              elevation: 5,
            }}>
            <TouchableOpacity
              onPress={() => {
                setShowModal(false);
              }}>
              <Icon name="times" color="black" size={20} />
            </TouchableOpacity>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 20,
              }}>
              <TouchableOpacity
                style={{
                  backgroundColor: 'green',
                  borderRadius: 10,
                  paddingVertical: 5,
                  alignItems: 'center',
                  width: '45%',
                }}
                onPress={() => {
                  setShowModal(false);
                  navigation.navigate('AddData', selected);
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>Edit</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setShowModal(false);
                  deleteData(selected);
                  getDataMobil();
                }}
                style={{
                  backgroundColor: 'red',
                  borderRadius: 10,
                  paddingVertical: 5,
                  alignItems: 'center',
                  width: '45%',
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>Delete</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <Modal visible={showAdd} transparent={true}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.4)',
          }}>
          <View style={{backgroundColor: '#fff', width: '70%'}}>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() => setShowAdd(false)}
                style={{
                  width: '10%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 10,
                }}>
                <Icon name="times" size={20} color="#000" />
              </TouchableOpacity>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
                Tambah Data
              </Text>
            </View>
            <View
              style={{
                width: '100%',
                padding: 15,
              }}>
              <View>
                <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                  Nama Mobil
                </Text>
                <TextInput
                  placeholder="Masukkan Nama Mobil"
                  value={namaMobil}
                  onChangeText={text => setNamaMobil(text)}
                  style={styles.txtInput}
                />
              </View>
              <View style={{marginTop: 20}}>
                <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                  Total Kilometer
                </Text>
                <TextInput
                  value={totalKM}
                  onChangeText={text => setTotalKM(text)}
                  placeholder="contoh: 100 KM"
                  style={styles.txtInput}
                />
              </View>
              <View style={{marginTop: 20}}>
                <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                  Harga Mobil
                </Text>
                <TextInput
                  value={hargaMobil}
                  onChangeText={text => setHargaMobil(text)}
                  placeholder="Masukkan Harga Mobil"
                  style={styles.txtInput}
                  keyboardType="number-pad"
                />
              </View>
              <TouchableOpacity
                disabled={disableBtn()}
                onPress={postData}
                style={[
                  styles.btnAdd,
                  disableBtn() ? {backgroundColor: '#A5A5A5'} : {},
                ]}>
                <Text style={{color: '#fff', fontWeight: '600'}}>
                  Tambah Data
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default Home;
